#!/usr/bin/env python3

# code partially taken from https://gist.github.com/c060604

import argparse
from PIL import Image
from PIL.ExifTags import TAGS
import piexif
import pandas as pd
import datetime
import re
from fractions import Fraction
import gpxpy
import gpxpy.gpx
import sys

def parse_arguments():
    parser = argparse.ArgumentParser(description='Geotag photos from a gps file')
    parser.add_argument('--img', type=str,nargs="+",required=True,help='Image file')
    parser.add_argument('--gps', type=str,nargs="+",required=True,help='GPS logger or gpx files')
    parser.add_argument('--format', type=str,required=True,help='gps format [txt; gpx]')
    parser.add_argument('--timedelta', type=int, required=True,help='Timedelta respect utc in hours')
    parser.add_argument('--maxdelta', type=int, default=1200,help='Maximum difference with gps timepoint in seconds')
    #parser.add_argument('--input2', metavar="input2", type=str, nargs=1, help='input single arg')
    #parser.add_argument('--option', action='store_true', help='option')
    args = parser.parse_args()
    return args

def get_labeled_exif(exif):
    labeled = {}
    for (key, val) in exif.items():
        labeled[TAGS.get(key)] = val
    return labeled

def load_gps(fname,file_format):
    if file_format=='txt':
        gps = pd.read_csv(fname,sep="\t",header=None,names=['year','month','day','hour','minute','second','lat','long'])
        gps['datetime'] = pd.to_datetime(gps[['year','month','day','hour','minute','second']])
        gps = gps.drop(['year','month','day','hour','minute','second'],axis=1)
    elif file_format=='gpx':
        gpx_file = open(fname,'r')
        gpx = gpxpy.parse(gpx_file)
        data = []
        for track in gpx.tracks:
            for segment in track.segments:
                for point in segment.points:
                    data.append({'datetime':point.time,'lat':point.latitude,'long':point.longitude})
        gps = pd.DataFrame.from_records(data)
        gps['datetime']=gps['datetime'].apply(remove_timezone)
    return gps

def remove_timezone(dt):
    return dt.replace(tzinfo=None)

def to_deg(value, loc):
    """convert decimal coordinates into degrees, munutes and seconds tuple

    Keyword arguments: value is float gps-value, loc is direction list ["S", "N"] or ["W", "E"]
    return: tuple like (25, 13, 48.343 ,'N')
    """
    if value < 0:
        loc_value = loc[0]
    elif value > 0:
        loc_value = loc[1]
    else:
        loc_value = ""
    abs_value = abs(value)
    deg =  int(abs_value)
    t1 = (abs_value-deg)*60
    min = int(t1)
    sec = round((t1 - min)* 60, 5)
    return (deg, min, sec, loc_value)


def change_to_rational(number):
    """convert a number to rational

    Keyword arguments: number
    return: tuple like (1, 2), (numerator, denominator)
    """
    f = Fraction(str(number))
    return (f.numerator, f.denominator)


def set_gps_location(file_name, lat, lng):
    """Adds GPS position as EXIF metadata

    Keyword arguments:
    file_name -- image file
    lat -- latitude (as float)
    lng -- longitude (as float)

    """
    lat_deg = to_deg(lat, ["S", "N"])
    lng_deg = to_deg(lng, ["W", "E"])

    exiv_lat = (change_to_rational(lat_deg[0]), change_to_rational(lat_deg[1]), change_to_rational(lat_deg[2]))
    exiv_lng = (change_to_rational(lng_deg[0]), change_to_rational(lng_deg[1]), change_to_rational(lng_deg[2]))

    gps_ifd = {
        piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
        piexif.GPSIFD.GPSLatitudeRef: lat_deg[3],
        piexif.GPSIFD.GPSLatitude: exiv_lat,
        piexif.GPSIFD.GPSLongitudeRef: lng_deg[3],
        piexif.GPSIFD.GPSLongitude: exiv_lng,
    }

    exif_dict = {"GPS": gps_ifd}

    # get original exif data first!
    exif_data = piexif.load(file_name)
    # update original exif data to include GPS tag
    try:
        exif_data['GPS'] = exif_dict
        exif_bytes = piexif.dump(exif_data)
    except:
        exif_data.update(exif_dict)
        exif_bytes = piexif.dump(exif_data)
    piexif.insert(exif_bytes, file_name)
    
def get_datetime(fname):
    im = Image.open(fname)
    exif_dict = piexif.load(im.info["exif"])
    dt = exif_dict['Exif'][36867]
    m = re.match('(....):(..):(..) (..):(..):(..)',str(dt,'utf-8'))
    dto = datetime.datetime(int(m.group(1)),int(m.group(2)),int(m.group(3)),int(m.group(4)),int(m.group(5)), int(m.group(6)),tzinfo=datetime.timezone(datetime.timedelta(hours=args.timedelta)))
    dto = dto.astimezone(datetime.timezone.utc)
    dto = dto.replace(tzinfo=None)
    return dto
        
if __name__ == "__main__":
    args = parse_arguments()
    try:
        assert args.format in ['txt','gpx']
    except AssertionError:
        print("Invalid gps file format parameter")
        sys.exit(1)
    # load gps data
    data = []
    for fname in args.gps:
        data.append(load_gps(fname,args.format))
    data = pd.concat(data)
    # process image files
    for fname in args.img:
        dt = get_datetime(fname)
        print(fname,dt)
        data['imgtime'] = dt
        data['delta'] = (data['imgtime']-data['datetime']).abs()
        bestmatch = data[data.delta == data.delta.min()]
        if bestmatch.delta.dt.total_seconds().values[0] < args.maxdelta:
            print(fname, bestmatch.lat.values[0], bestmatch.long.values[0])
            set_gps_location(fname, bestmatch.lat.values[0], bestmatch.long.values[0])
