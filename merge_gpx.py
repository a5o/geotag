#!/usr/bin/env python3

import argparse
import gpxpy
import gpxpy.gpx

def parse_arguments():
    parser = argparse.ArgumentParser(description='Template script')
    parser.add_argument('--gpx', type=str,nargs="+",required=True,help='GPX input  files')
    parser.add_argument('--out', type=str,required=True,help='GPX output file')
    args = parser.parse_args()
    return args

def load_gpx(fname,gpx_all):
    gpx_file = open(fname,'r')
    gpx = gpxpy.parse(gpx_file)
    data = []
    for track in gpx.tracks:
        gpx_track = gpxpy.gpx.GPXTrack()
        gpx_all.tracks.append(gpx_track)
        for segment in track.segments:
            gpx_segment = gpxpy.gpx.GPXTrackSegment()
            gpx_track.segments.append(gpx_segment)
            for point in segment.points:
                gpx_segment.points.append(point)

if __name__ == "__main__":
    args = parse_arguments()
    gpx = gpxpy.gpx.GPX()
    for fname in args.gpx:
        load_gpx(fname,gpx)
    out_file = open(args.out,'w+')
    out_file.write(gpx.to_xml())
    out_file.close()
