#!/usr/bin/env python3

# code partially taken from https://gist.github.com/c060604

import argparse
from PIL import Image
from PIL.ExifTags import TAGS
import piexif
import pandas as pd
import datetime
import re
from fractions import Fraction
import gpxpy
import gpxpy.gpx
import sys

def parse_arguments():
    parser = argparse.ArgumentParser(description='Geotag photos from a gps file')
    parser.add_argument('--img', type=str,nargs="+",required=True,help='Image file')
    parser.add_argument('--lat', type=float,required=False,help='Latitude')
    parser.add_argument('--long', type=float,required=False,help='Longitude')
    parser.add_argument('--copy_from', type=str, required=False,help='Copy location from image file')
    #parser.add_argument('--input2', metavar="input2", type=str, nargs=1, help='input single arg')
    #parser.add_argument('--option', action='store_true', help='option')
    args = parser.parse_args()
    return args

def get_labeled_exif(exif):
    labeled = {}
    for (key, val) in exif.items():
        labeled[TAGS.get(key)] = val
    return labeled

def remove_timezone(dt):
    return dt.replace(tzinfo=None)

def to_deg(value, loc):
    """convert decimal coordinates into degrees, munutes and seconds tuple

    Keyword arguments: value is float gps-value, loc is direction list ["S", "N"] or ["W", "E"]
    return: tuple like (25, 13, 48.343 ,'N')
    """
    if value < 0:
        loc_value = loc[0]
    elif value > 0:
        loc_value = loc[1]
    else:
        loc_value = ""
    abs_value = abs(value)
    deg =  int(abs_value)
    t1 = (abs_value-deg)*60
    min = int(t1)
    sec = round((t1 - min)* 60, 5)
    return (deg, min, sec, loc_value)


def change_to_rational(number):
    """convert a number to rational

    Keyword arguments: number
    return: tuple like (1, 2), (numerator, denominator)
    """
    f = Fraction(str(number))
    return (f.numerator, f.denominator)

def get_gps_location(fname):
    im = Image.open(fname)
    exif_dict = piexif.load(im.info["exif"])
    gps = exif_dict['GPS']
    return gps 

def set_gps_location(file_name, lat, lng):
    """Adds GPS position as EXIF metadata

    Keyword arguments:
    file_name -- image file
    lat -- latitude (as float)
    lng -- longitude (as float)

    """
    lat_deg = to_deg(lat, ["S", "N"])
    lng_deg = to_deg(lng, ["W", "E"])

    exiv_lat = (change_to_rational(lat_deg[0]), change_to_rational(lat_deg[1]), change_to_rational(lat_deg[2]))
    exiv_lng = (change_to_rational(lng_deg[0]), change_to_rational(lng_deg[1]), change_to_rational(lng_deg[2]))

    gps_ifd = {
        piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
        piexif.GPSIFD.GPSLatitudeRef: lat_deg[3],
        piexif.GPSIFD.GPSLatitude: exiv_lat,
        piexif.GPSIFD.GPSLongitudeRef: lng_deg[3],
        piexif.GPSIFD.GPSLongitude: exiv_lng,
    }

    exif_dict = {"GPS": gps_ifd}

    # get original exif data first!
    exif_data = piexif.load(file_name)
    # update original exif data to include GPS tag
    #exif_data['GPS'] = exif_dict
    exif_data.update(exif_dict)
    exif_bytes = piexif.dump(exif_data)
    piexif.insert(exif_bytes, file_name)
    
if __name__ == "__main__":
    args = parse_arguments()
    if args.copy_from:
        source_gps = get_gps_location(args.copy_from)
        for fname in args.img:
            print(fname)
            dest_gps = get_gps_location(fname)
            exif_data = piexif.load(fname)
            try:
                exif_data['GPS'] = source_gps
                exif_bytes = piexif.dump(exif_data)
            except:
                exif_data.update(source_gps)
                exif_bytes = piexif.dump(exif_data)
            piexif.insert(exif_bytes, fname)
            final_gps = get_gps_location(fname)
    else:
        if args.lat and args.long:
            for fname in args.img:
                print(fname)
                set_gps_location(fname, args.lat, args.long)


